
$(document).ready(function () {
    $('.hero-effect').sakura();

    var isOpenMenu = false;

    $(".countdown").countdowntimer({
        dateAndTime : "2018/11/03 14:45:00",
		size : "lg",
        displayFormat : "DHMS",
        regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
        regexpReplaceWith: "" +
        "<div class='countdonw-block'>" +
            "<div class='time-block'>$1 <div class='time-section'>dní</div> </div>" +
            "<div class='time-block'>$2 <div class='time-section'>hodín</div> </div>" +
            "<div class='time-block'>$3 <div class='time-section'>minút</div> </div>" +
            "<div class='time-block'>$4 <div class='time-section'>sekúnd</div> </div>" +
        "</div>"
    });

    $('.slider-box').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        fade: true,
        speed: 4000
    });

    $('.hero-slider-box').slick({
        autoplay: true,
        arrows: false,
        dots: false,
        fade: true,
        speed: 2500
    });

    $('.menu-item').click(function () {
        $('.header-navigation-toggle').removeClass('active');
        isOpenMenu = false;
    });

    $('.header-navigation-toggle').click(function () {
        if(isOpenMenu) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        isOpenMenu = !isOpenMenu;
    });
    var headerHeight = 70;
    $(".smooth-scroll").click(function(e) {
        event.preventDefault();
        var addressValue = $(this).attr("href");
        $('html, body').animate({
            scrollTop: $(addressValue).offset().top - headerHeight
        }, 1000);
    });

    window.onscroll = function() {scrollSite();};
    var sticky = $('#navbar').offset().top;
    var infoSection = $('#header').offset().top;
    var aboutSection = $('#about-us').offset().top-headerHeight;
    var whenSection = $('#when-where').offset().top-headerHeight;
    var forYoungSection = $('#for-young').offset().top-headerHeight;

    scrollSite();
    function scrollSite() {
        if (window.pageYOffset >= sticky) {
            $('#navbar').addClass("floating");
        } else {
            $('#navbar').removeClass("floating");
        }
        console.log(window.pageYOffset);
        addAnimations();

        if (window.pageYOffset >= forYoungSection) {
            $('.header-navigation li.young').addClass("hovered");
            $('.header-navigation li.about').removeClass("hovered");
            $('.header-navigation li.intro').removeClass("hovered");
            $('.header-navigation li.when').removeClass("hovered");
        } else if (window.pageYOffset >= whenSection) {
            $('.header-navigation li.when').addClass("hovered");
            $('.header-navigation li.about').removeClass("hovered");
            $('.header-navigation li.intro').removeClass("hovered");
            $('.header-navigation li.young').removeClass("hovered");
        } else if (window.pageYOffset >= aboutSection) {
            $('.header-navigation li.about').addClass("hovered");
            $('.header-navigation li.intro').removeClass("hovered");
            $('.header-navigation li.when').removeClass("hovered");
            $('.header-navigation li.young').removeClass("hovered");
        } else if (window.pageYOffset >= infoSection) {
            $('.header-navigation li.intro').addClass("hovered");
            $('.header-navigation li.about').removeClass("hovered");
            $('.header-navigation li.when').removeClass("hovered");
            $('.header-navigation li.young').removeClass("hovered");
        } else {
            $('.header-navigation li.about').removeClass("hovered");
            $('.header-navigation li.intro').removeClass("hovered");
            $('.header-navigation li.when').removeClass("hovered");
            $('.header-navigation li.young').removeClass("hovered");
        }
    }




    function addAnimations() {

        if( window.outerWidth >= 768 ) {
            var scrollFirstSection = 300;

            var groomTitleSection = 1000;
            var groomContentSection = 1100;

            var timeTitleSection = 2000;
            var timeContentSection = 2100;

            var youngTitleSection = 3300;
            var youngContentSection = 3400;

        } else if ( window.outerWidth < 768 && window.outerWidth > 480 ) {
            var scrollFirstSection = 170;

            var groomTitleSection = 1130;
            var groomContentSection = 1250;

            var timeTitleSection = 2530;
            var timeContentSection = 2640;

            var youngTitleSection = 4280;
            var youngContentSection = 4440;

        } else {
            var scrollFirstSection = 230;

            var groomTitleSection = 1150;
            var groomContentSection = 1260;

            var timeTitleSection = 2530;
            var timeContentSection = 2640;

            var youngTitleSection = 4280;
            var youngContentSection = 4450;
        }

        if(window.pageYOffset >= youngContentSection) {
            $('.young-content-section').addClass('jackrose-animate');
        }
        if (window.pageYOffset >= youngTitleSection) {
            $('.young-title-section').addClass('jackrose-animate');
        }
        if(window.pageYOffset >= timeContentSection) {
            $('.time-content-section').addClass('jackrose-animate');
        }
        if (window.pageYOffset >= timeTitleSection) {
            $('.time-title-section').addClass('jackrose-animate');
        }
        if(window.pageYOffset >= groomContentSection) {
            $('.groom-content-section').addClass('jackrose-animate');
        }
        if (window.pageYOffset >= groomTitleSection) {
            $('.groom-title-section').addClass('jackrose-animate');
        }
        if(window.pageYOffset >= scrollFirstSection){
            $('.first-section').addClass('jackrose-animate');
        }
        if(window.pageYOffset >= scrollFirstSection){
            $('.first-section').addClass('jackrose-animate');
        }
    }
});
