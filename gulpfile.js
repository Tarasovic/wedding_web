var gulp = require("gulp");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var jshint = require("gulp-jshint");
var autoprefixer = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var uglify = require("gulp-uglify");
var browserSync = require("browser-sync").create();
var sourcemaps = require("gulp-sourcemaps");

/**
 * Kompiluje sass subory
 */
gulp.task("sass", function() {
  return gulp
    .src([__dirname + "/assets/css/style.sass"])
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer("last 10 versions"))
    .pipe(cleanCSS())
    .pipe(concat("style.min.css"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(__dirname + "/web/css"));
});

/**
 * Kompiluje js subory
 */
gulp.task("js", function() {
  return gulp
    .src([__dirname + "/assets/js/index.js"])
    .pipe(sourcemaps.init())
    .pipe(jshint())
    .pipe(jshint.reporter("default"))
    .pipe(uglify())
    .pipe(concat("index.min.js"))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(__dirname + "/web/js"));
});

/**
 * Sleduje subory
 */
gulp.task("watch", function() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    files: [
      __dirname + "/web/css/*",
      __dirname + "/web/js/*",
      __dirname + "/*"
    ]
  });
  gulp.watch("assets/css/**/*.sass", gulp.series(["sass"]));
  gulp.watch("assets/css/**/*.scss", gulp.series(["sass"]));
  //gulp.watch(__dirname + "/assets/js/plugins/**/*", ["vendor-js"]);
  gulp.watch("assets/js/*.js", gulp.series(["js"]));
});

gulp.task("default", gulp.series(["watch"]));
